#!/usr/bin/bash

set -euET -o pipefail

ligo compile contract examples/add_number/add_number.jsligo
ligo compile contract examples/charity/charity.jsligo
ligo run test examples/charity/charity_test.jsligo
ligo compile contract examples/counter/counter.jsligo
ligo compile contract examples/counter_two_numbers/counter_two_numbers.jsligo
ligo compile contract examples/verifications/account.jsligo
ligo compile contract examples/visitors/visitors.jsligo
ligo compile contract examples/visitors/visitors.jsligo
ligo compile contract exercises/auction_flaws/auction.jsligo
ligo run test exercises/auction_flaws/auction_test.jsligo
ligo compile contract exercises/timelock_flaw/timelock.jsligo
ligo compile contract exercises/timelock_flaw/timelock.jsligo

# Test cheat sheet, this runs arbitrary commands from the cheat sheet HTML, either audit them ahead of time or run this in an isolated environment (preferably GitPod)
(
  sudo apt install -y expect
  d="$(date +%s)";
  mkdir -p ".cheat-sheet-jsligo-test/$d"; # TODO: use mktemp -d
  ln -nsf "$d" ".cheat-sheet-jsligo-test/last-test";
  cd ".cheat-sheet-jsligo-test/$d";
  cat > ./repl <<'EOF'
#!/usr/bin/env expect
set timeout 20
spawn ligo repl jsligo
# wait for prompt
expect "1"
send "[exec cat [lindex $argv 0]];;\n\x04"
interact
puts stderr "\n"
EOF
  chmod +x repl
  cat > ./run_bash_euET-opipefail <<'EOF'
sed -i -e '1i #!/bin/bash\
set -euET -o pipefail' "$1"
chmod +x "$1"
./"$1"
EOF
  chmod +x ./run_bash_euET-opipefail
  to='/dev/null';
  grep '<!-- \(\|to: .*\|echo: .*\|run: .*\)-->' ../../cheat-sheet-jsligo.html \
  | while read ab; do
    if test "x${ab:0:8}" = 'x<!-- to:'; then
      to="$(printf %s "$ab" | sed -e 's/^<!-- to: //; s/ -->$//')";
    elif test "x${ab:0:9}" = 'x<!-- run:'; then
      run="$(printf %s "$ab" | sed -e 's/^<!-- run: //; s/ -->$//')";
      echo "run: to=$to; $run"
      (eval "$run") </dev/null;
      old_to="$to";
      to='/dev/null'
    elif test "x${ab:0:10}" = 'x<!-- echo:'; then
      if test "x$to" = "x/dev/null"; then
        printf %s\\n "cheat-sheet-jsligo.html : must open a file with <!-- to: filename.jsligo --> before lines of code."
        printf %s\\n "$ab"
        exit 1
      fi
      printf %s\\n "$ab" | sed -e 's/^<!-- echo://; s/ -->//; s/<em title="\([^"]*\)">[^>]*>/\1/g' | sed -e 's/&lt;/</g; s/&gt;/>/g; s/&quot;/"/g; s/&amp;/&/g' >> $to;
    else
      if test "x$to" = "x/dev/null"; then
        printf %s\\n "cheat-sheet-jsligo.html : must open a file with <!-- to: filename.jsligo --> before lines of code."
        printf %s\\n "$ab"
        exit 1
      fi
      printf %s\\n "$ab" | sed -e 's/^<!-- -->//; s/<em title="\([^"]*\)">[^>]*>/\1/g' | sed -e 's/&lt;/</g; s/&gt;/>/g; s/&quot;/"/g; s/&amp;/&/g' >> $to;
    fi;
  done
)
